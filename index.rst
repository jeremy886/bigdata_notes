Jeremy886's PyData Study Notes
==============================

.. toctree::
   :glob:
   :maxdepth: 2
   :caption: Contents:

   about
   course_list
   pandas/*
   visualisation/*
   python_skills/*